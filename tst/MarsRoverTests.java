import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/************************************************************************************
 * a MarsRover should be given a command, then execute all moves in that command.
 * at the end it should be able to give its location.
 *
 * command structure : [drop-location]_[move-commands]
 *
 * location : X.Y.D
 *              with 'X' & 'Y' being integers representing a coordinate in 'move-units'.
 *              with 'D' being a direction : 'N'orth, 'E'ast, 'S'outh, 'W'est
 *
 * move-commands :
 *              'F'orward : rover moves 1 move-unit forward (not changing its facing)
 *              'B'ackward : rover moves 1 unit backwards (not changing its facing)
 *              'L'eft : rover pivots counterclockwise (as seen from above & not changing position)
 *              'R'ight : rover pivots clockwise (as seen from above & not changing position)
 *
 * move unit : the rover operates on a 2D grid.  it can only move whole units from
 *              1 coordinate to another.
 *
 * example : MarsRover.drop("2.3.N_LFFRBBB") should put the rover at "0.0.N".
 ************************************************************************************/
public class MarsRoverTests {
    @Test
    void When_dropped_RoverLocation_SHOULD_BE_drop_location(){
        Assertions.assertEquals("0.0.N", MarsRover.drop("0.0.N"));
    }
}
